<?php

namespace App\Utilities\Session;

class SessionManager
{

    /**
    *
    * @param $name
    * @param $arguments
    * @return mixed
    */
    public static function __callStatic($name, $arguments){

        $session = static::className();

        return call_user_func_array([new $session, $name], $arguments);
    }

    /**
    *
    * @return string
    */
    private static function className(){

       $driver = DotEnv::get('SESSION_DRIVER', 'file');

       return __NAMESPACE__ . "\\" . ucfirst($driver) . "Session";
    }

}

