<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class AuthController extends Controller
{
    public function checkAuth() {


        return response()->json(['user' => Session::get("token")]);
    }

    public function register(Request $request){
        $request->validate([
            'name' => 'required|',
            'email' => 'required|email|unique:users',
            'profileImage' => 'required',
            'password' => 'required|min:6',
            'phone' => 'required|numeric',
            'address' => 'required',
        ]);
        $input = $request->all();
        $image = str_replace("data:image/png;base64,", "", $request->profileImage);

        $decode_image = base64_decode($image);
        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'profileImage' => $decode_image,
            'password' => Hash::make($request->password),
            'phone' => $request->phone,
            'address' => $request->address
        ]);

        $user->save();
        $token = $user->createToken($user->email.'-'.now());


        return response()->json(['user' => $user, 'token' => $token->accessToken]);
    }

    public function login(Request $request){
        $request->validate([
            //'email' => 'required|email|exists:users,email',
            'email' => 'required|email|',
            'password' => 'required|'
        ]);
        //$request->session()->put("token", $token->accessToken)
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            $user=Auth::user();
            $user_image = clone $user;
            $user_image->profileImage = base64_encode($user->profileImage);
            $token=$user->createToken($user->email.'-'.now());

            Session::put("token", $token->accessToken);

            return response()->json([
                'token' => $token->accessToken,
                'user' => $user_image,
                'message' => "authorized",
                'test' => Session::get("token")
            ]);
        } else {
            return response()->json(['message'=>"unauthorized"]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request/*, $id*/)
    {
        $request->validate([
            'loggedinUserEmail' => 'required',
        ]);

        $image = str_replace("data:image/png;base64,", "", $request->profileImage);

        $decode_image = base64_decode($image);

        $users = User::all();
        $loggedinuser;
        foreach ($users as $user):
        {
            if ($user->email == $request->loggedinUserEmail){
                $loggedinuser = $user;
                break;
            }
        } endforeach;

        $loggedinuser->name = $request->name;
        $loggedinuser->email = $request->email;
        $loggedinuser->password = Hash::make($request->password);
        $loggedinuser->profileImage = $decode_image;
        $loggedinuser->phone = $request->phone;
        $loggedinuser->address = $request->address;
        $token = $loggedinuser->createToken($loggedinuser->email.'-'.now());
        $user = $loggedinuser;
        $loggedinuser->update();

        $user->save();
        //return response()->json(['user' => $user, 'token' => $token->accessToken]);
        return response()->json([
                    $loggedinuser->name, $loggedinuser->email, $loggedinuser->phone, $loggedinuser->address
                ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
