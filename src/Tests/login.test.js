import {Global as jest} from "@jest/types/build/Global";

const fetch = require('./LogIn')


// jest.spyOn(auth, 'loggedIn').mockReturnValue(true);
// const wrapper = mount(<Footer />);
// expect(wrapper.text()).toEqual('XYZ');
test('login test', () => {

    return fetch.handleSubmit(res => res.json())
        .then(res => expect(res.result).toMath('{"data":{"token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZTViZWUxN2U4YjZiMmYyZmZlNjU1YmYyNTg2ZDZmNmU3MTJlNDliMDZiOWY2MTJiOWQwZDY2MGY0OTJkMDExMWVkYzAxOGI0MDYzY2YyYmYiLCJpYXQiOjE1OTA0NzYwODAsIm5iZiI6MTU5MDQ3NjA4MCwiZXhwIjoxNjIyMDEyMDgwLCJzdWIiOiIxMyIsInNjb3BlcyI6W119.c0PpIkK9zg2f_aBrV0XrylYnBlfJ6qdsy2cvBOlC8TJUJ-Iqvfpd59k1pGhUni17C0sHGkwy-dA1EYHfrt9cjDe0XQD7cG-vvZjQf64Z6H1rW3R_cXMbkD0Hegrap12gqld1fA860Lm7RHXH6H_FcptCezSOz894iXb_xrzrhqA1aSZeKi783ma2OKjWPfIBCYt6pc0OZNcJ27lacbhDjENmWYpQp-p6XQVEeNXJ9byzwsSnAHULCz48xSxoa3L8YkB8RucPSneZGGjC6gXGcj1DMTJKCZpirC5ee__XLJxdXlBXcqsQR-2zY8I89p1gxFKeXgMxhXMfaHnq2UOu_4f-BpqBeaqFT-uM4u2wv9Gm0TQEO6cv7Gne0zb2WoYno9KzSvcqwLbZrKHgXyQb5WA3FpFyd5fne-Gn2JbI7SQxe6TaHwTjCX6fK5OMIyNAL0QkwCGup0nKi-HIVULPTpK1Fxx45d6q-XJtyJh5HP3YR0nypJ3OzStPV8uJfmofquPrRiQwMH8Yh41kbXv0PPH4SENh408IyMLXW8acJXMsWqh199v0PGr6OeTlUqZcc6p9lnheuOfFs-Bhaqy_Aq_whZLAjkOUXTAqCMeZ0mWdMQBfwasmFiJPzZN2dXAKZDViTgpszTnXa8r_-EyjtCv5quOVUeyhwK_54-HsgHs","user":{"id":13,"name":"Aitculesei Andrei","email":"andrei.aitculesei@yahoo.com","email_verified_at":null,"created_at":"2020-05-05T09:36:23.000000Z","updated_at":"2020-05-05T09:36:23.000000Z","profileImage":"","phone":"0744284365","address":"Str.Teodor Mihali Nr. 4"}},"status":200,"statusText":"OK","headers":{"cache-control":"no-cache, private","content-type":"application/json"},"config":{"url":"http://127.0.0.1:8000/api/login","method":"post","data":"{\\"email\\":\\"andrei.aitculesei@yahoo.com\\",\\"password\\":\\"password\\"}","headers":{"Accept":"application/json, text/plain, */*","Content-Type":"application/json;charset=utf-8"},"transformRequest":[null],"transformResponse":[null],"timeout":0,"xsrfCookieName":"XSRF-TOKEN","xsrfHeaderName":"X-XSRF-TOKEN","maxContentLength":-1},"request":{}}'))
})
