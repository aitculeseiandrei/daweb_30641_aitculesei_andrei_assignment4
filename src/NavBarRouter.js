import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./Home";
import News from "./News";
import About from "./About";
import StudentProfile from "./StudentProfile";
import CoordinatorProfile from "./CoordinatorProfile";

const NavBarRouter = (props) => {
    return (
        <Switch>
            <Route exact path="/">
                <Home />
            </Route>
            <Route exact path="/Home">
                <Home />
            </Route>
            <Route exact path="/News">
                <News />
            </Route>
            <Route exact path="/About">
                <About />
            </Route>
            <Route exact path="/StudentProfile">
                <StudentProfile />
            </Route>
            <Route exact path="/CoordinatorProfile">
                <CoordinatorProfile />
            </Route>
        </Switch>
    );
}

export default NavBarRouter;
