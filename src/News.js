import React, {useState} from 'react';
import "./index.css"
import {Redirect} from "react-router-dom";

function  News() {

        let content1 = {
            English: {
                text1: "Good choice!",
                text2: "Here you will find the news related to the project!",
                text3: "Android News + ONVIF protocol!",
                text4: "IOS News + ONVIF protocol!",
                text5: "Bibliography!",
                text6: "Delete table!",
                text9: "Schedule",
                text10: "Monday",
                text11: "Tuesday",
                text12: "Friday",
                text13: "Saturday",
                text14: "Sunday",
                text15: "closed",
                text16: "Note",
                text17: "The MONDAY afternoon program is during the academic year, except for vacations.",
                text18: "Address",
            },
            Romanian: {
                text1: "Buna alegere!",
                text2: "Aici o sa gasiti noutatile legate de lucrarea de licenta!",
                text3: "Noutati Android + protocolul ONVIF!",
                text4: "Noutati IOS + protocolul ONVIF!",
                text5: "Bibliografie!",
                text6: "Sterge tabel!",
                text9: "Orar",
                text10: "Luni",
                text11: "Marti",
                text12: "Vineri",
                text13: "Sambata",
                text14: "Duminica",
                text15: "inchis",
                text16: "Nota",
                text17: "Programul de LUNI dupa-masa este pe durata anului unversitar, cu exceptia vacantelor.",
                text18: "Adrese",
            }
        }

        let languageStoredInLocalStorage = localStorage.getItem("language");
        let [language, setLanguage] = useState(
            languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
        );

        let content;
        (language === 'Romanian')
            ? (content = content1.Romanian)
            : (content = content1.English)

        const token = localStorage.getItem("token");
        if(!token){
            return <Redirect to="/"/>
        }

        return (

            <div className="News">

                <br/>
                <div className="Container">
                    <div id="slides" className="carousel slide carousel-fade" data-ride="carousel">
                        <ul className="carousel-indicators">
                            <li data-target="#slides" data-slide-to="0" className="active"/>
                            <li data-target="#slides" data-slide-to="1"/>
                        </ul>

                        <div className="carousel-inner">
                            <div className="carousel-item active">
                                <img src={require("./img/android.gif")} alt="Acasa"/>

                            </div>
                            <div className="carousel-item">
                                <img src={require("./img/IOS.gif")} alt="News"/>

                            </div>
                        </div>
                    </div>

                </div>


                <div className="container-fluid padding">
                    <div className="row welcome text-center">
                        <div className="col-12">
                            <h1 className="display-4"><b> {content.text1} </b></h1>

                        </div>
                        <hr/>
                        <div className="col-12">
                            <p className="lead"> {content.text2} </p>

                        </div>
                    </div>
                </div>

                <div id="Android" className="tabcontent">
                    <h3><i className="fab fa-android" aria-hidden="true"/> Android</h3>
                    <p> {content.text3} </p>
                </div>

                <div id="IOS" className="tabcontent">
                    <h3><i className="fab fa-apple" aria-hidden="true"/> IOS</h3>
                    <p> {content.text4} </p>
                </div>
                <div className="clearfix"/>

                <button onClick={function f() {

                    var xmlContent = '';
                    var tableBooks = document.getElementById('books');
                    if (tableBooks == null)
                        document.location.reload();
                    else{
                    fetch("./Noutati.xml").then((response) => {
                        response.text().then((xml) => {
                            xmlContent = xml;
                            var parser = new DOMParser();
                            var xmlDOM = parser.parseFromString(xmlContent, 'application/xml');
                            var books = xmlDOM.querySelectorAll('book');

                            books.forEach(bookXmlNode => {
                                var row = document.createElement('tr');

                                //author
                                var td = document.createElement('td');
                                td.innerText = bookXmlNode.children[0].innerHTML;
                                row.appendChild(td);

                                //title
                                td = document.createElement('td');
                                td.innerText = bookXmlNode.children[1].innerHTML;
                                row.appendChild(td);

                                //publish date
                                td = document.createElement('td');
                                td.innerText = bookXmlNode.children[3].innerHTML;
                                row.appendChild(td);

                                //description
                                td = document.createElement('td');
                                td.innerText = bookXmlNode.children[4].innerHTML;
                                row.appendChild(td);

                                tableBooks.children[1].appendChild(row);
                            });
                        });

                    });
                }}}> {content.text5} </button>

                <button onClick={function fc() {
                    var tableBooks = document.getElementById('books');
                    if(tableBooks != null)
                        tableBooks.remove();
                }}> {content.text6} </button>
                <br/><hr/>
                <footer>
                    <div className="container-fluid padding">
                        <div className="row text-center">
                            <div className="col-md-4">
                                <img src={require("./img/loggo.png")}/>
                                <hr className="light"/>
                                <p> +40-(0)264-401218 </p>
                                <p> utcn@cs.utcluj.ro </p>
                                <p> G. Baritiu 26-28, Sala 48 </p>
                                <p> Cluj-Napoca, Cluj, Romania </p>
                            </div>
                            <div className="col-md-4">
                                <hr className="light"/>
                                <h5> {content.text9} </h5>
                                <hr className="light"/>
                                <p> {content.text10}: 08:00-9:00, 11:00-14:00, 16:00-19:00 </p>
                                <p> {content.text11}-{content.text12}: 08:00-09:00, 11:00-14:00 </p>
                                <p> {content.text13}-{content.text14}: {content.text15} </p>
                                <p><b>{content.text16}:</b> {content.text17} </p>
                            </div>
                            <div className="col-md-4">
                                <hr className="light"/>
                                <h5> {content.text18} </h5>
                                <hr className="light"/>
                                <p><b>B-dul Muncii:</b> 400641, Cluj-Napoca, Romania </p>
                                <p><b>Strada Memorandumului:</b> 400114, Cluj-Napoca, Romania </p>
                                <p><b>George Baritiu:</b> 400027, Cluj-Napoca, Romania </p>
                            </div>
                            <div className="col-12">
                                <hr className="light-100"/>
                                <h5>&copy; prezentariLicente.ro </h5>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }



export default News;

