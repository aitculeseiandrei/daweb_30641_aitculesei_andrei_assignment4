/*global google*/

import React from 'react'
import  { compose, withProps, lifecycle } from 'recompose'
import {withScriptjs, withGoogleMap, GoogleMap, DirectionsRenderer} from 'react-google-maps'
import {GoogleApiWrapper, Map, Marker, Polyline} from "google-maps-react";
const polyline = require('@mapbox/polyline');

const mapStyles = {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    width: '70%',
    height: '70%'
};

let YOUR_LATITUDE;
let YOUR_LONGITUDE;
navigator.geolocation.watchPosition(function(position) {
    YOUR_LATITUDE = position.coords.latitude;
    YOUR_LONGITUDE = position.coords.longitude;
});

class MyMapComponent extends React.Component {
    constructor(props){
        super(props)

        this.state = {
            name: "React",
            coords: [],
        };
    }render() {
        // const origin = "41.8507300, -87.6512600";
        // const destination = "41.8525800, -87.6514100";
        const origin = ""+YOUR_LATITUDE+","+YOUR_LONGITUDE;
        const {coords} = this.state;
        // const startLocation = "43.032495, 22.045352";
        const destination = "46.482556,24.096638";
        const DirectionsComponent = compose(
            withProps({

                googleMapURL: `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=AIzaSyC52C1iZzl61EZOb1STkrhBSwT03Hzc-sk`,
                loadingElement: <div style={{ height: `400px` }} />,
                containerElement: <div style={{ width: `100%` }} />,
                mapElement: <div style={{height: `600px`, width: `600px` }}  />,
            }),
            withScriptjs,
            withGoogleMap,
            lifecycle({
                componentDidMount() {
                    const DirectionsService = new google.maps.DirectionsService();
                    DirectionsService.route({
                        origin: new google.maps.LatLng(41.8507300, -87.6512600),
                        destination: new google.maps.LatLng(41.8525800, -87.6514100),
                        travelMode: google.maps.TravelMode.DRIVING,
                    }, (result, status) => {
                        if (status === google.maps.DirectionsStatus.OK) {this.setState({
                            directions: {...result},
                            markers: true
                        })
                        } else {
                            console.error(`error fetching directions ${result}`);
                        }
                    });
                }}
            //     async componentDidMount() {
            //         // if (navigator.geolocation) {
            //         //     navigator.geolocation.watchPosition(function(position) {
            //         //         YOUR_LATITUDE = 43.032495;//position.coords.latitude;
            //         //         YOUR_LONGITUDE = 22.045352;//position.coords.longitude;
            //         //     });
            //         // }
            //         const origin = ""+YOUR_LATITUDE+","+YOUR_LONGITUDE;
            //
            //         // const startLocation = "43.032495, 22.045352";
            //         const destination = "46.482556,24.096638";
            //         try {
            //             const proxyurl = "https://cors-anywhere.herokuapp.com/";
            //             const resp = await fetch(proxyurl+`https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=AIzaSyC52C1iZzl61EZOb1STkrhBSwT03Hzc-sk`);
            //             const respJson = await resp.json();
            //
            //             if (respJson.routes.length > 0) {
            //                 let points = polyline.decode(respJson.routes[0].overview_polyline.points);
            //                 let coords = points.map((point, index) =>{
            //                     return {
            //                         latitude: point[0],
            //                         longitude: point[1]
            //                     };
            //                 });
            //                 this.setState({coords: coords});
            //             } else {
            //                 console.log(respJson.routes.length);
            //             }
            //             // return;
            //         } catch (e) {
            //             console.log('error', e);
            //         }
            //     }
            // }
                )
        )(props =>
            <Map
                defaultZoom={3}
                google={this.props.google}
                zoom={14}
                style={mapStyles}
                initialCenter={{
                    lat: YOUR_LATITUDE,
                    lng: YOUR_LONGITUDE
                }}
            >
                <Polyline
                    coordinates={coords}
                    strokeColor = "red"
                    strokeWidth = {1}
                />
                <Marker
                    onClick={this.onMarkerClick}
                    name={'This is test name'}
                />
                {props.directions && <DirectionsRenderer directions={props.directions} suppressMarkers={props.markers}/>}
            </Map>
        );return (
            <DirectionsComponent
            />
        )
    }
}
// export default MyMapComponent
export default GoogleApiWrapper({
    apiKey: 'AIzaSyC52C1iZzl61EZOb1STkrhBSwT03Hzc-sk'
})(MyMapComponent);
