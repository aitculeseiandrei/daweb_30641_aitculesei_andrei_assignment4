import React, {useState} from 'react';

import "./index.css";
import {Redirect} from "react-router-dom";

const Home = (props) => {



    let content1 = {
        English: {
            text8: "Welcome!",
            text9: "Here you gonna find anything about the final project!",
            text10: "Built with the last version of HTML, HTML5.",
            text11: "Built with the last version of Bootstrap, Bootstrap 4.",
            text12: "Built with the last version of CSS, HTML5.",
            text13: "Schedule",
            text14: "Monday",
            text15: "Tuesday",
            text16: "Friday",
            text17: "Saturday",
            text18: "Sunday",
            text19: "closed",
            text20: "Note",
            text21: "The MONDAY afternoon program is during the academic year, except for vacations.",
            text22: "Address",
        },
        Romanian: {
            text8: "Bine ati venit!",
            text9: "Aici o sa gasiti orice informatie doriti in legatura cu lucrarea de" +
                "                                licenta!",
            text10: "Contruit cu ultima versiune HTML, HTML5.",
            text11: "Contruit cu ultima versiune Bootstrap, Bootstrap 4.",
            text12: "Contruit cu ultima versiune CSS, HTML5.",
            text13: "Orar",
            text14: "Luni",
            text15: "Marti",
            text16: "Vineri",
            text17: "Sambata",
            text18: "Duminica",
            text19: "inchis",
            text20: "Nota",
            text21: "Programul de LUNI dupa-masa este pe durata anului unversitar, cu exceptia vacantelor.",
            text22: "Adrese",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)

    var tableBooks = document.getElementById('books');
    if (tableBooks != null)
        tableBooks.remove();

    const token = localStorage.getItem("token");
    if(!token){
        return <Redirect to="/"/>
    }

    return (

        <div className="Home">

            <div className="Container">
                <div id="slides" className="carousel slide carousel-fade" data-ride="carousel">
                    <ul className="carousel-indicators">
                        <li data-target="#slides" data-slide-to="0" className="active"></li>
                        <li data-target="#slides" data-slide-to="1" ></li>
                        <li data-target="#slides" data-slide-to="2" ></li>
                        <li data-target="#slides" data-slide-to="3" ></li>
                        <li data-target="#slides" data-slide-to="4" ></li>
                        <li data-target="#slides" data-slide-to="5" ></li>
                    </ul>

                    <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src={require("./img/homePage.png")} alt="Acasa"/>

                        </div>
                        <div className="carousel-item">
                            <img src={require("./img/newsPage.png")} alt="News"/>

                        </div>
                        <div className="carousel-item">
                            <img src={require("./img/aboutPage.png")} alt="Despre"/>

                        </div>
                        <div className="carousel-item">
                            <img src={require("./img/studentInfoPage.png")} alt="Student"/>

                        </div>
                        <div className="carousel-item">
                            <img src={require("./img/coordonatorPage.png")} alt="Coordonator"/>
                        </div>
                        <div className="carousel-item">
                            <img src={require("./img/contactPage.jpg")} alt="Contact"/>
                        </div>
                    </div>
                </div>

            </div>

            <div className="container-fluid padding ">
                <div className="row welcome text-center">
                    <div className="col-12 align-content-center">
                        <h1 className="display-4"><b> {content.text8} </b></h1>

                    </div>
                    <hr/>
                    <div className="col-12 align-content-center">
                        <p className="lead"> {content.text9} </p>

                    </div>
                </div>
            </div>

            <div className="container-fluid padding">
                <div className="row text-center padding">
                    <div className="col-xs-12 col-sm-6 col-md-4">
                        <i className="fas fa-code"/>
                        <h3>HTML5</h3>
                        <p>{content.text10}</p>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-4">
                        <i className="fas fa-bold"/>
                        <h3>BOOTSTRAP</h3>
                        <p>{content.text11}</p>
                    </div>
                    <div className="col-xs-12 col-sm-6 col-md-4">
                        <i className="fab fa-css3"/>
                        <h3>CSS3</h3>
                        <p>{content.text12}</p>
                    </div>
                </div>
                <hr className="my-4"/>
            </div>
            <footer>
                <div className="container-fluid padding">
                    <div className="row text-center">
                        <div className="col-md-4">
                            <img src={require("./img/loggo.png")}/>
                            <hr className="light"/>
                            <p> +40-(0)264-401218 </p>
                            <p> utcn@cs.utcluj.ro </p>
                            <p> G. Baritiu 26-28, Sala 48 </p>
                            <p> Cluj-Napoca, Cluj, Romania </p>
                        </div>
                        <div className="col-md-4">
                            <hr className="light"/>
                            <h5> {content.text13} </h5>
                            <hr className="light"/>
                            <p> {content.text14}: 08:00-9:00, 11:00-14:00, 16:00-19:00 </p>
                            <p> {content.text15}-{content.text16}: 08:00-09:00, 11:00-14:00 </p>
                            <p> {content.text17}-{content.text18}: {content.text19} </p>
                            <p><b>{content.text20}:</b> {content.text21} </p>
                        </div>
                        <div className="col-md-4">
                            <hr className="light"/>
                            <h5> {content.text22} </h5>
                            <hr className="light"/>
                            <p><b>B-dul Muncii:</b> 400641, Cluj-Napoca, Romania </p>
                            <p><b>Strada Memorandumului:</b> 400114, Cluj-Napoca, Romania </p>
                            <p><b>George Baritiu:</b> 400027, Cluj-Napoca, Romania </p>
                        </div>
                        <div className="col-12">
                            <hr className="light-100"/>
                            <h5>&copy; prezentariLicente.ro </h5>
                        </div>
                    </div>
                </div>
            </footer>

        </div>
    );
}

export default Home;
