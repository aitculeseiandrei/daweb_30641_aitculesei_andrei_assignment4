

import React, {Component, useState} from "react";
import { Link, Redirect } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import Routes from "./Routes";
import axios from "axios";


import "./App.css";
import Navigation from "./NavLanguage";

function App() {

    let content1 = {
        English: {
            text1: "Log in",
            text2: "Register",
            text3: "Main",
            text4: "Log out"
        },
        Romanian: {
            text1: "Logare",
            text2: "Inregistrare",
            text3: "Meniu",
            text4: "Delogare",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)


    axios.get('http://127.0.0.1:8000/api/checkAuth')
        .then(function (response) {
            // handle success
            console.log("LOGAT!");
            console.log(response);
            return (
                <div className="App container">

                    <Navigation
                        language={language}
                        handleSetLanguage={language => {
                            setLanguage(language);
                            storeLanguageInLocalStorage(language);
                        }}/>
                    <br/><br/>
                    <nav className="navbar navbar-expand-md navbar-light bg-light sticky-top ">
                        <Navbar>
                            <Navbar.Header>
                                <Navbar.Brand>
                                    <Link to="/NavigationBar">{content.text3}</Link>
                                </Navbar.Brand>
                                <Navbar.Toggle/>
                            </Navbar.Header>
                        </Navbar>
                        <Navbar fluid collapseOnSelect>

                            <Navbar.Collapse>
                                <Nav pullRight>
                                    <LinkContainer to="/" >
                                        <NavItem onClick={function logOut() {
                                            localStorage.clear("token");
                                            document.location.reload();
                                        }}>{content.text4}</NavItem>
                                    </LinkContainer>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                    </nav>
                    <Routes/>

                </div>
            );
        })
        .catch(function (error) {
            // handle error
            console.log("DELOGAT!");
            console.log(error);
            return (
                <div className="App container">

                    <Navigation
                        language={language}
                        handleSetLanguage={language => {
                            setLanguage(language);
                            storeLanguageInLocalStorage(language);
                        }}/>
                    <br/><br/>
                    <nav className="navbar navbar-expand-md navbar-light bg-light sticky-top ">
                        <Navbar>
                            <Navbar.Header>
                                <Navbar.Brand>
                                    <Link to="/">{content.text3}</Link>
                                </Navbar.Brand>
                                <Navbar.Toggle/>
                            </Navbar.Header>
                        </Navbar>
                        <Navbar fluid collapseOnSelect>

                            <Navbar.Collapse>
                                <Nav pullRight>
                                    <LinkContainer to="/register">
                                        <NavItem>{content.text2}</NavItem>
                                    </LinkContainer>
                                    <LinkContainer to="/login">
                                        <NavItem>{content.text1}</NavItem>
                                    </LinkContainer>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                    </nav>
                    <Routes/>

                </div>
            );
        })
}

function storeLanguageInLocalStorage(language) {
    document.location.reload();
    localStorage.setItem("language", language);
};

export default App;

