import React, {Component, useState} from "react";
import Navigation from "./NavLanguage";
import {Link, Redirect} from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import NavBarRouter from "./NavBarRouter"


import "./index.css";
import {Map, Marker} from "google-maps-react";
// import Home from "./Home";

function NavigationBar() {

    const mapStyles = {
        width: '100%',
        height: '100%'
    };

    let content1 = {
        English: {
            text1: "Home",
            text2: "News",
            text3: "About project",
            text4: "Student profile",
            text5: "Coordinator profile",
            text6: "Address",
            text7: "Phone",
            text8: "Welcome!",
            text9: "Schedule",
            text10: "Monday",
            text11: "Tuesday",
            text12: "Friday",
            text13: "Saturday",
            text14: "Sunday",
            text15: "closed",
            text16: "Note",
            text17: "The MONDAY afternoon program is during the academic year, except for vacations.",
            text18: "Address",
            text19: "View my location",
        },
        Romanian: {
            text1: "Acasa",
            text2: "Noutati",
            text3: "Despre lucrare",
            text4: "Profil Student",
            text5: "Profil coordonator",
            text6: "Adresa",
            text7: "Telefon",
            text8: "Bine ati venit!",
            text9: "Orar",
            text10: "Luni",
            text11: "Marti",
            text12: "Vineri",
            text13: "Sambata",
            text14: "Duminica",
            text15: "inchis",
            text16: "Nota",
            text17: "Programul de LUNI dupa-masa este pe durata anului unversitar, cu exceptia vacantelor.",
            text18: "Adrese",
            text19: "Locatia mea",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)

    const userEmail = localStorage.getItem("userEmail");
    const userAddress = localStorage.getItem("userAddress");
    const userPhone = localStorage.getItem("userPhone");
    const token = localStorage.getItem("token");
    if(!token){
        return <Redirect to="/"/>
    }

    let YOUR_LATITUDE;
    let YOUR_LONGITUDE;

    function componentDidMount() {
        if (navigator.geolocation) {
            navigator.geolocation.watchPosition(function(position) {
                YOUR_LATITUDE = position.coords.latitude;
                YOUR_LONGITUDE = position.coords.longitude;
            });
        }
    }

    // function componentDidMount() {
    //     if ("geolocation" in navigator) {
    //         console.log("Available");
    //     } else {
    //         console.log("Not Available");
    //     }
    // }

    return (
        <div className="App container" >

            <Navigation
                language={language}
                handleSetLanguage={language => {
                    setLanguage(language);
                    storeLanguageInLocalStorage(language);
                }}/>

            <br/><br/><br/>
            <ul className="nav justify-content-center">
                <a className="navbar-brand" href="#"> <img src={require("./img/loggo.png")} alt={"UTCN"}/> </a>
            </ul>
            <br/>

            <nav className="navbar navbar-expand-md navbar-light bg-light sticky-top ">
                <div className="container-fluid collapse navbar-collapse" id="navbarResponsive">
                    <Navbar className="nav justify-content-center navbar-nav">
                        <Navbar.Collapse>
                            <Nav>
                                <LinkContainer to={"/home"} >
                                    <NavItem className="nav-item nav-link" ><i className="fa fa-fw fa-home"/> {content.text1}</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/news">
                                    <NavItem className="nav-item nav-link"><i className="fa fa-fw fa-search"/> {content.text2}</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/about">
                                    <NavItem className="nav-item nav-link"><i className="fa fa-fw fa-question"/> {content.text3}</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/studentProfile">
                                    <NavItem className="nav-item nav-link"><i className="fa fa-fw fa-user"/> {content.text4}</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/coordinatorProfile">
                                    <NavItem className="nav-item nav-link"><i className="fa fa-fw fa-user"/> {content.text5}</NavItem>
                                </LinkContainer>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"><i className="fa fa-fw fa-envelope"/> Contact </a>
                                    <div className="dropdown-menu">
                                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-home"/>{content.text6 + " :"} <p> {userAddress} </p></a> {/*Str.Teodor Mihali Nr. 4*/}
                                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-phone"/> {content.text7 + " :"}<p> {userPhone} </p></a> {/*0744284365*/}
                                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-envelope"/> Mail: <p> {userEmail} </p></a> {/*andrei.aitculesei1@gmail.com*/}
                                    </div>
                                </li>
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>

                </div>
            </nav>
            <NavBarRouter />
            <button onClick={function componentDidMount(){

                window.location.href = "./LocationOnMap";
            }}>
                {content.text19}
            </button>
            <footer>
                <div className="container-fluid padding">
                    <div className="row text-center">
                        <div className="col-md-4">
                            <img src={require("./img/loggo.png")}/>
                            <hr className="light"/>
                            <p> +40-(0)264-401218 </p>
                            <p> utcn@cs.utcluj.ro </p>
                            <p> G. Baritiu 26-28, Sala 48 </p>
                            <p> Cluj-Napoca, Cluj, Romania </p>
                        </div>
                        <div className="col-md-4">
                            <hr className="light"/>
                            <h5> {content.text9} </h5>
                            <hr className="light"/>
                            <p> {content.text10}: 08:00-9:00, 11:00-14:00, 16:00-19:00 </p>
                            <p> {content.text11}-{content.text12}: 08:00-09:00, 11:00-14:00 </p>
                            <p> {content.text13}-{content.text14}: {content.text15} </p>
                            <p><b>{content.text16}:</b> {content.text17} </p>
                        </div>
                        <div className="col-md-4">
                            <hr className="light"/>
                            <h5> {content.text18} </h5>
                            <hr className="light"/>
                            <p><b>B-dul Muncii:</b> 400641, Cluj-Napoca, Romania </p>
                            <p><b>Strada Memorandumului:</b> 400114, Cluj-Napoca, Romania </p>
                            <p><b>George Baritiu:</b> 400027, Cluj-Napoca, Romania </p>
                        </div>
                        <div className="col-12">
                            <hr className="light-100"/>
                            <h5>&copy; prezentariLicente.ro </h5>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}
function storeLanguageInLocalStorage(language) {
    // document.location.reload();
    localStorage.setItem("language", language);
};

export default NavigationBar;
