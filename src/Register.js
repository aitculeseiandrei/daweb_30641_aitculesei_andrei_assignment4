import React, {Component, useState} from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Register.css";
import Navigation from "./NavLanguage";
import axios from "axios";
import {Redirect} from "react-router-dom"

const textStyle = {color: 'black', };

function Register() {

    let content1 = {
        English: {
            text1: "Register",
            text2: "Name",
            text3: "Password",
            text4: "Invalid datas, please try again!",
            test15: "Confirm password",
            text6: "Phone",
            text7: "Address",
            text8: "Account created witch success!",
        },
        Romanian: {
            text1: "Inregistrare",
            text2: "Nume",
            text3: "Parola",
            text4: "Date invalide, va rugam incercati din nou!",
            test15: "Confirmati parola",
            text6: "Telefon",
            text7: "Adresa",
            text8: "Cont creat cu succes!"
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );
    let registerFailMessage = "";
    let registerSuccessMessage = "";

    let content;
    if (language === 'Romanian'){
        content = content1.Romanian;
        registerFailMessage = "Date invalide, va rugam incercati din nou!";
        registerSuccessMessage = "Cont creat cu succes!";
    } else {
        content = content1.English;
        registerFailMessage = "Invalid datas, please try again!";
        registerSuccessMessage = "Account created witch success!";
    }

    const [email, setEmail] = useState("");
    const [name, setUsername] = useState("");
    const [password, setPassword] = useState("");
    let profileImage = '';
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");

    function validateForm() {
        return name.length > 0 && email.length > 0 && password.length > 0 && phone.length > 0 && address.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();
        axios.post("http://127.0.0.1:8000/api/register", {name: name, email: email, profileImage: profileImage, password: password, phone: phone, address: address});
    }

    const token = localStorage.getItem("token");
    if(token){
        document.location.reload();
        return <Redirect to="/NavigationBar"/>
    }

    return (

        <div className="Register">

            <Navigation
                language={language}
                handleSetLanguage={language => {
                    setLanguage(language);
                    storeLanguageInLocalStorage(language);
                }}/>

            <form onSubmit={handleSubmit}>
                <FormGroup controlId="username" bsSize="large">
                    <ControlLabel style={textStyle}>{content.text2}</ControlLabel>
                    <FormControl
                        autoFocus
                        type="username"
                        value={name}
                        onChange={e => setUsername(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="email" bsSize="large">
                    <ControlLabel style={textStyle}>Email</ControlLabel>
                    <FormControl
                        autoFocus
                        type="email"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="password" bsSize="large">
                    <ControlLabel style={textStyle}>{content.text3}</ControlLabel>
                    <FormControl
                        value={password}
                        onChange={e => setPassword(e.target.value)}
                        type="password"
                    />
                </FormGroup>
                <FormGroup controlId="phone" bsSize="large">
                    <ControlLabel style={textStyle}>{content.text6}</ControlLabel>
                    <FormControl
                        value={phone}
                        onChange={e => setPhone(e.target.value)}
                        type="phone"
                    />
                </FormGroup>
                <FormGroup controlId="address" bsSize="large">
                    <ControlLabel style={textStyle}>{content.text7}</ControlLabel>
                    <FormControl
                        value={address}
                        onChange={e => setAddress(e.target.value)}
                        type="address"
                    />
                </FormGroup>
                <FormGroup>
                    <br/><input type="file" onChange={function uploadImage(event){
                        const imageFile = event.target.files[0];
                        let reader = new FileReader();
                        reader.readAsDataURL(imageFile);
                        reader.onload=(e)=>{
                            profileImage = e.target.result;
                            console.log("img data ", profileImage);
                        }
                }}/>
                </FormGroup><br/>
                <Button block bsSize="large" disabled={!validateForm()} type="submit" style={textStyle}>
                    {content.text1}!
                </Button>
            </form>
        </div>
    );
}
function storeLanguageInLocalStorage(language) {
    document.location.reload();
    localStorage.setItem("language", language);
};

export default Register;
