import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./Home";
import News from "./News";
import About from "./About";
import StudentProfile from "./StudentProfile";
import CoordinatorProfile from "./CoordinatorProfile";
import LogIn from "./LogIn";
import Register from "./Register";
import NavigationBar from "./NavigationBar";
import Update from "./update";
import LocationOnMap from "./LocationOnMap";
import MyMapComponent from "./MyMapComponent";

const Routes = (props) => {
    return (
        <Switch>
            <Route exact path="/login">
                <LogIn />
            </Route>
            <Route exact path="/register">
                <Register />
            </Route>
            <Route exact path="/NavigationBar">
                <NavigationBar />
            </Route>
            <Route exact path="/Home">
                <Home />
            </Route>
            <Route exact path="/News">
                <News />
            </Route>
            <Route exact path="/About">
                <About />
            </Route>
            <Route exact path="/StudentProfile">
                <StudentProfile />
            </Route>
            <Route exact path="/CoordinatorProfile">
                <CoordinatorProfile />
            </Route>
            <Route exact path="/update">
                <Update />
            </Route>
            <Route exact path="/LocationOnMap">
                <LocationOnMap />
            </Route>
            <Route exact path="/MyMapComponent">
                <MyMapComponent />
            </Route>

        </Switch>
    );
}

export default Routes;
