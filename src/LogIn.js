import React, {Component, useState} from "react";
import {Button, FormGroup, FormControl, ControlLabel} from "react-bootstrap";
import "./LogIn.css";
import "./NavigationBar.js";
import {LinkContainer} from "react-router-bootstrap";
import axios from "axios";
import {Redirect} from "react-router-dom";

const textStyle = {color: 'black', };



function LogIn() {
// export default class LogIn extends React.Component {
//     constructor(props) {
//         super(props)
//         this.state = {
//             email: '',
//             password: '',
//             language: '',
//             isLogined: false
//         }
//     }
//     render() {
        let content1 = {
            English: {
                text1: "Login",
                text2: "Password",

            },
            Romanian: {
                text1: "Logare",
                text2: "Parola",

            }
        }

        const [email, setEmail] = useState("");
        const [password, setPassword] = useState("");

        let languageStoredInLocalStorage = localStorage.getItem("language");
        let [language, setLanguage] = useState(
            languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
        );

        // if (languageStoredInLocalStorage.localeCompare("English") === 0){
        //     // eslint-disable-next-line react/no-direct-mutation-state
        //     this.state.laguage = "English"
        // } else {
        //     this.state.laguage = "Romanian"
        // }

        let content;
        (language === 'Romanian')
            ? (content = content1.Romanian)
            : (content = content1.English)

        function validateForm() {
            return email.length > 0 && password.length > 0;
        }

        function handleSubmit(event) {
            event.preventDefault();

            axios.post("http://127.0.0.1:8000/api/login", {email: email, password: password})
                .then(res => {

                    console.log("response", res.data.message);

                    if (res.data.message.localeCompare("authorized") === 0){

                        localStorage.setItem("token", res.data.token);
                        localStorage.setItem("userName", res.data.user.name);
                        localStorage.setItem("userEmail", res.data.user.email);
                        localStorage.setItem("userImage", res.data.user.profileImage);
                        localStorage.setItem("userAddress", res.data.user.address);
                        localStorage.setItem("userPhone", res.data.user.phone);
                        alert("Hello " + res.data.user.name + "!");

                        document.location.reload();
                        return <Redirect to="/NavigationBar"/>
                    } else {
                        alert("Unable to log in with provided credentials.");
                    }
                })
        }

        const token = localStorage.getItem("token");
        if (token) {
            document.location.reload();
            return <Redirect to="/NavigationBar"/>
        }

        return (
            <div className="LogIn">
                <form onSubmit={handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel style={textStyle}>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={email}
                            onChange={e => setEmail(e.target.value)}
                            /* eslint-disable-next-line react/no-direct-mutation-state */
                            // onChange={e => e.target.value}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel style={textStyle}>{content.text2}</ControlLabel>
                        <FormControl
                            value={password}
                            onChange={e => setPassword(e.target.value)}
                            /* eslint-disable-next-line react/no-direct-mutation-state */
                            // onChange={e => e.target.value}
                            type="password"
                        />
                    </FormGroup>
                    <Button block bsSize="large" disabled={!validateForm()} type="submit"
                            style={textStyle}>
                        {content.text1}!
                    </Button>
                </form>

            </div>
        );
    // }
}

export default LogIn;
